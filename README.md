# Technical challenge

The source code herein is my submission for the Glint technical challenge.

I elected to complete this challenge with minimal 3rd party libraries bar a few essentials to get me setup and running quickly, some decisions I made were...
* ParcelJS as my bundler, it's like webpack but zero config, gives me a good environmet for developing React apps quickly.
* No `;` Based on https://standardjs.com I like the look of JS this way, it is fun to do when it's a personal project.
* Class component and functional component with hooks, I wanted to show I understand both ways to write a React component, and I like having my main component as a Class.
* Simple responsive layout, I didn't want to spend too much time on the visuals, but I added a media query to make the layout more useable at larger screen sizes.

If I was to cache the list of users for offline display I would consider using the web LocalStorage API, as it has decent support currently and works quickly, if the data size was going to be even bigger I would consider indexDB, but for this example LocalStorage should be fine.

My favourite libraries are probably the Core React library it's changed the landscape of front-end development like no other, and makes creating interactive web apps much more straigthforward, I am also a fan of ParcelJS which is a zero config bundler, it really helps me get started on a new project with a nice modern development environment with minimal fuss.

Top 5 tools I can't live without are probably Firefox for development tools, Vim as it can be a really full featured IDE with the right plugins and is available in some form everywhere, a good notebook and pen are essential, MDN is the best reference for any queries I may have in JavaScript or web standards, and that's it really, I like to keep my requirements and dependencies low, this keeps me flexible and able to adapt and work in any environment.