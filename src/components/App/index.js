import React from 'react'
import UserListItem from '/components/UserListItem'

const API_URL = 'https://api.github.com/users?per_page=50'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null,
      error: null,
      isLoading: false
    }
  }

  componentDidMount() {
    this.setState({isLoading: true})

    fetch(API_URL)
      .then(response => {
        if (response.ok) {
          return response.json()
        } else {
          throw new Error('Something went wrong!')
        }
      })
      .then(data => {
        this.setState({
          data: data,
          isLoading: false
        })
      })
      .catch(error => {
        this.setState({
          error: error,
          isLoading: false
        })
      })
  }

  render() {
    const { data, error, isLoading } = this.state

    if (error) {
      return <p>{error.message}</p>
    }

    if (isLoading) {
      return <p>Loading...</p>
    }

    return (
      <div className="component-app">
        <ul>
          {data != null && (
              data.map(item =>
              <UserListItem
                avatar={item.avatar_url}
                followersURL={item.followers_url}
                key={item.id}
                login={item.login}
            />
          ))}
        </ul>
      </div>
    )
  }
}

export default App
