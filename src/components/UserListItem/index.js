import React, { useState } from 'react'

const UserListItem = (props) => {

  const [followers, setFollowers] = useState()
  const [isLoading, setIsLoading] = useState(false)

  const getFollowers = () => {
    setIsLoading(true)
    fetch(props.followersURL)
      .then((response) => response.json())
      .then((data) => {
        setFollowers(data)
        setIsLoading(false)
      })
  }

  const avatar = (!props.avatar) ? null : (
    <img src={props.avatar} alt={'avatar for user ' + props.login}/>
  );
  const followersList = (followers == undefined) ? null : followers.map(follower => {
    return <span key={follower.id}>{follower.login}, </span>
  })
  const loadingMessage = (isLoading) ? <p>Loading followers</p> : null;
  return (
    <li>
      {avatar}
      <div>
        <h2>{props.login}</h2>
        <button onClick={getFollowers}>Followers &gt;</button>
        <div>
          {loadingMessage}
          {followersList}
        </div>
      </div>
    </li>
  )
}

export default UserListItem
